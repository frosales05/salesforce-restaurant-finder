@IsTest
public class RestaurantFinderTest {
    private static final String NEW_CLIENT_ID = '123456789987654321';
    private static final String NEW_CLIENT_SECRET = '987654321123456789';
    private static final String BASE_URL = 'https://api.foursquare.com/';
    private static final String ENDPOINT = 'v2/venues/search?';
    private static final String API_VERSION = '20190221';
    private static final String COGENT_LABS_LATITUDE_LONGITUDE = '35.6487634,139.7021532';
    private static final String RESTAURANT_CATEGORY = '4d4b7105d754a06374d81259';
    private static final String TEAM_DEVELOPER_NAME = RestaurantFinder.TEAM_DEVELOPER_NAME_PREFIX + 'TESTING_TEAM';
    private static final String CREATED_TEAM_TEST_NAME = 'CREATED_TEAM';
    private static final String UPDATED_TEAM_TEST_NAME = 'UPDATED_TEAM';
    private static final String LUNCH_TIME = '12:00';
    private static final String QUERY = 'restaurant';
    private static final String RADIUS = '1000';
    private static final String ID_FOR_TESTING = '5642aef9498e51025cf4a7a5';

    @TestSetup
    static void testSetup(){
        RestaurantFinderSettings__c restaurantFinderSettings = new RestaurantFinderSettings__c(
        	Name = 'Name',
            Client_Id__c = '1111111111111',
            Client_Secret__c = '22222222222',
            API_Version__c = API_VERSION,
            Base_URL__c = BASE_URL,
            Category_Id__c = RESTAURANT_CATEGORY,
            Endpoint__c = ENDPOINT,
            Initial_Latitude_Longitude__c = COGENT_LABS_LATITUDE_LONGITUDE
        );
        insert restaurantFinderSettings;

        User user1 = new User(
            LastName = 'TestUser1',
            Email = 'testuser1@fake.email',
            Alias = 'tstus1', 
            Username = String.valueOf(Datetime.now().getTime())+'testuser1@fake.email',
            TimeZoneSidKey = 'America/Santiago',
            LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LocaleSidKey = 'en_US',
            ProfileId = UserInfo.getProfileId(),
            RestaurantFinderTeam__c = TEAM_DEVELOPER_NAME
        );
        insert user1;

        User user2 = new User(
            LastName = 'TestUser2',
            Email = 'TestUser2@fake.email',
            Alias = 'tstus2',
            Username = String.valueOf(Datetime.now().getTime())+'testuser2@fake.email',
            TimeZoneSidKey = 'America/Santiago',
            LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LocaleSidKey = 'en_US',
            ProfileId = UserInfo.getProfileId(),
            RestaurantFinderTeam__c = TEAM_DEVELOPER_NAME
        );
        insert  user2;

        Group team1 = new Group(
            Name = 'TestGroup1',
            DeveloperName = TEAM_DEVELOPER_NAME
        );
        insert team1;

        System.runAs(user1){
            GroupMember teamMember1 = new GroupMember(
                UserOrGroupId = user1.Id,
                GroupId = team1.Id
            );
            insert teamMember1;

            GroupMember teamMember2 = new GroupMember(
                UserOrGroupId = user2.Id,
                GroupId = team1.Id
            );
            insert teamMember2;   
        }
    }

    @IsTest
    static void shouldReturnRestaurantOptions(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CallOutServiceMock(CallOutServiceMock.foursquareMockResponse(ID_FOR_TESTING),'application/json',200));
        System.assertEquals(ID_FOR_TESTING, RestaurantFinder.getRestaurantOptions(QUERY, RADIUS)[0].get('id'));
        Test.stopTest();
    }

    @IsTest
    static void shouldInsertARestaurantOption(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CallOutServiceMock(CallOutServiceMock.foursquareMockResponse(ID_FOR_TESTING),'application/json',200));
        RestaurantFinder.saveRestaurantOption(formatRestaurantOptions(RestaurantFinder.getRestaurantOptions(QUERY, RADIUS))[0]);
        System.assert(![SELECT Id FROM RestaurantOption__c].IsEmpty());
        Test.stopTest();
    }

    @IsTest
    static void shouldReturnTrueIfTheUserHasSelectedARestaurantOption(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CallOutServiceMock(CallOutServiceMock.foursquareMockResponse(ID_FOR_TESTING),'application/json',200));
        RestaurantFinder.saveRestaurantOption(formatRestaurantOptions(RestaurantFinder.getRestaurantOptions(QUERY, RADIUS))[0]);
        System.assertEquals(true, RestaurantFinder.checkIfTheUserAlreadyMadeAChoice());
        Test.stopTest();
    }

    @IsTest
    static void shouldReturnTrueIfTheUserIsTeamLeader(){
        Test.startTest();
        User teamLeader = RestaurantFinder.getActiveUsers(TEAM_DEVELOPER_NAME)[0];
        RestaurantFinder.markTeamLeader(teamLeader.Id, TEAM_DEVELOPER_NAME);
        User teamLeader2 = RestaurantFinder.getActiveUsers(TEAM_DEVELOPER_NAME)[1];
        RestaurantFinder.markTeamLeader(teamLeader2.Id, TEAM_DEVELOPER_NAME);
        System.runAs(teamLeader2){
            System.assertEquals(true, RestaurantFinder.checkIfTheUserIsTeamLEader());
        }
        Test.stopTest();
    }

    @IsTest
    static void shouldReturnRestaurantOptionsSelectedByTheTeam(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CallOutServiceMock(CallOutServiceMock.foursquareMockResponse(ID_FOR_TESTING),'application/json',200));
        RestaurantFinder.saveRestaurantOption(formatRestaurantOptions(RestaurantFinder.getRestaurantOptions(QUERY, RADIUS))[0]);
        System.assertEquals(false, RestaurantFinder.fetchRestaurantOptions().isEmpty());
        Test.stopTest();
    }

    @IsTest
    static void shouldInsertARestaurantChoiceAndRelateToItTheSelectedRestaurantOption(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CallOutServiceMock(CallOutServiceMock.foursquareMockResponse(ID_FOR_TESTING),'application/json',200));
        RestaurantFinder.saveRestaurantOption(formatRestaurantOptions(RestaurantFinder.getRestaurantOptions(QUERY, RADIUS))[0]);
        User teamLeader = [SELECT Id, RestaurantFinderTeam__c FROM User WHERE Id = : UserInfo.getUserId()];
        RestaurantOption__c restaurantSelected = [SELECT Id, RestaurantChoice__c FROM RestaurantOption__c WHERE RestaurantChoice__r.Team__c = :teamLeader.RestaurantFinderTeam__c];
        RestaurantFinder.saveRestaurantChoice(restaurantSelected.Id);
        System.assertEquals(restaurantSelected.Id, [SELECT RestaurantSelected__c FROM RestaurantChoice__c WHERE Id = :restaurantSelected.RestaurantChoice__c].RestaurantSelected__c);
        Test.stopTest();
    }

    @IsTest
    static void shouldReturnTrueIfTheLeaderAlreadySelectedAnRestaurant(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CallOutServiceMock(CallOutServiceMock.foursquareMockResponse(ID_FOR_TESTING),'application/json',200));
        RestaurantFinder.saveRestaurantOption(formatRestaurantOptions(RestaurantFinder.getRestaurantOptions(QUERY, RADIUS))[0]);
        User teamLeader = [SELECT Id, RestaurantFinderTeam__c FROM User WHERE Id = : UserInfo.getUserId()];
        RestaurantOption__c restaurantSelected = [SELECT Id, RestaurantChoice__c FROM RestaurantOption__c WHERE RestaurantChoice__r.Team__c = :teamLeader.RestaurantFinderTeam__c];
        RestaurantFinder.saveRestaurantChoice(restaurantSelected.Id);
        System.assertEquals(true, RestaurantFinder.checkIfTheLeaderAlreadyMadeAChoice());
        Test.stopTest();
    }

    @IsTest
    static void shouldReturnAdministrationSettings(){
        Test.startTest();
        System.assertNotEquals(null, RestaurantFinder.getAdministrationSettings());
        Test.stopTest();
    }

    @IsTest
    static void shouldUpdateAdministrationSettings(){
        Test.startTest();
        String settingsId = [SELECT Id FROM RestaurantFinderSettings__c LIMIT 1].Id;
        RestaurantFinder.updateAdministrationSettings(settingsId, NEW_CLIENT_ID, NEW_CLIENT_SECRET, API_VERSION, BASE_URL, RESTAURANT_CATEGORY, ENDPOINT, COGENT_LABS_LATITUDE_LONGITUDE, LUNCH_TIME);
        RestaurantFinderSettings__c updatedRestaurantFinderSettings = [SELECT Id, Client_Id__c, Client_Secret__c FROM RestaurantFinderSettings__c WHERE Id = :settingsId];
        System.assertEquals(NEW_CLIENT_ID, updatedRestaurantFinderSettings.Client_Id__c);
        System.assertEquals(NEW_CLIENT_SECRET, updatedRestaurantFinderSettings.Client_Secret__c);        
        Test.stopTest();
    }

    @IsTest
    static void shouldReturnAListOfActiveUsers(){
        Test.startTest();
        System.assert(!RestaurantFinder.getActiveUsers(TEAM_DEVELOPER_NAME).isEmpty());
        Test.stopTest();
    }

    @IsTest
    static void shouldReturnAListOfGroups(){
        Test.startTest();
        System.assert(!RestaurantFinder.getRestaurantFinderTeams().isEmpty());
        Test.stopTest();
    }

    @IsTest
    static void shouldReturnAListOfGroupMembers(){
        Test.startTest();
        System.assert(!RestaurantFinder.getTeamMembersByTeamDeveloperName(TEAM_DEVELOPER_NAME).isEmpty());
        Test.stopTest();
    }

    @IsTest
    static void shouldCreateATeamWithGivenName(){
        Test.startTest();
        RestaurantFinder.createTeam(CREATED_TEAM_TEST_NAME);
        System.assertEquals(RestaurantFinder.TEAM_DEVELOPER_NAME_PREFIX + CREATED_TEAM_TEST_NAME, [SELECT DeveloperName FROM Group WHERE Name = :CREATED_TEAM_TEST_NAME].DeveloperName);
        Test.stopTest();
    }

    @IsTest
    static void shouldUpdateATeamWithGivenName(){
        Test.startTest();
        RestaurantFinder.createTeam(CREATED_TEAM_TEST_NAME);
        System.assertEquals(RestaurantFinder.TEAM_DEVELOPER_NAME_PREFIX + UPDATED_TEAM_TEST_NAME, RestaurantFinder.updateTeam([SELECT DeveloperName FROM Group WHERE Name = :CREATED_TEAM_TEST_NAME].DeveloperName, UPDATED_TEAM_TEST_NAME));
        Test.stopTest();
    }

    @IsTest
    static void shouldDeleteATeamWithGivenName(){
        Test.startTest();
        RestaurantFinder.deleteTeam(TEAM_DEVELOPER_NAME);
        System.assert([SELECT Id FROM Group WHERE DeveloperName = :TEAM_DEVELOPER_NAME].isEmpty());
        Test.stopTest();
    }

    @IsTest
    static void shouldTeamMembers(){
        Test.startTest();
        List<String> teamMemberIds = new List<String>();
        for(User user : RestaurantFinder.getActiveUsers(TEAM_DEVELOPER_NAME)){
            teamMemberIds.add(user.Id);
        }
        RestaurantFinder.createTeamMembers(teamMemberIds, TEAM_DEVELOPER_NAME);
        System.assert(![SELECT Id FROM GroupMember WHERE UserOrGroupId IN :teamMemberIds].isEmpty());
        Test.stopTest();
    }

    @IsTest
    static void shouldMarkTeamLeader(){
        Test.startTest();
        User teamLeader = RestaurantFinder.getActiveUsers(TEAM_DEVELOPER_NAME)[0];
        RestaurantFinder.markTeamLeader(teamLeader.Id, TEAM_DEVELOPER_NAME);
        User teamLeader2 = RestaurantFinder.getActiveUsers(TEAM_DEVELOPER_NAME)[1];
        RestaurantFinder.markTeamLeader(teamLeader2.Id, TEAM_DEVELOPER_NAME);
        System.assertEquals(true, [SELECT RestaurantFinderTeamLeader__c FROM User WHERE Id = :teamLeader2.Id].RestaurantFinderTeamLeader__c);
        Test.stopTest();
    }

    @IsTest
    static void shouldReturnMarkTeamLeader(){
        Test.startTest();
        User teamLeader = RestaurantFinder.getActiveUsers(TEAM_DEVELOPER_NAME)[0];
        teamLeader.RestaurantFinderTeamLeader__c = true;
        teamLeader.RestaurantFinderTeam__c = TEAM_DEVELOPER_NAME;
        update teamLeader;
        System.assertEquals(teamLeader.Id, RestaurantFinder.getTeamLeader(TEAM_DEVELOPER_NAME)[0].Id);
        Test.stopTest();
    }

    @IsTest
    static void shouldReturnTeamStatus(){
        Test.startTest();
        Map<String,Boolean> teamStatus = RestaurantFinder.getTeamStatus(TEAM_DEVELOPER_NAME);
        System.assertEquals(true, teamStatus.get('NoTeamLeaderYet'));
        System.assertEquals(false, teamStatus.get('LessThan2TeamMembers'));
        Test.stopTest();
    }

    @IsTest
    static void shouldDeleteTeamMembers(){
        Test.startTest();
        String idToDelete = RestaurantFinder.getActiveUsers(TEAM_DEVELOPER_NAME)[0].Id;
        RestaurantFinder.dropTeamMembers(
            new List<String>{
                idToDelete
            }
        );
        List<String> currentMembers = new List<String>();
        for(User user : RestaurantFinder.getTeamMembersByTeamDeveloperName(TEAM_DEVELOPER_NAME)){
            currentMembers.add(user.Id);
        }
        System.assert([SELECT Id FROM User WHERE Id IN :currentMembers AND Id = :idToDelete].isEmpty());
        Test.stopTest();
    }

    @IsTest
    static void shouldScheduleAJobToNotifyTheTeamMembersWhenAdministrationDataIsChanged(){
        Test.startTest();
        List<CronTrigger> jobs = [SELECT Id FROM CronTrigger];
        if(jobs != null && !jobs.isEmpty()){
            for(CronTrigger job : jobs){
             	System.abortJob(job.Id);   
            }
        }
        RestaurantFinder.scheduleNotification(LUNCH_TIME);
        RestaurantFinderSettings__c restaurantFinderSettings = [SELECT Id, Notifier_Job_Id__c FROM RestaurantFinderSettings__c LIMIT 1];
        System.assertEquals(false, [SELECT Id FROM CronTrigger WHERE Id = :restaurantFinderSettings.Notifier_Job_Id__c].isEmpty());
        Test.stopTest();
    }

    private static List<Map<Object,Object>> formatRestaurantOptions(List<Map<String,Object>> restaurantOptions){
        List<Map<Object,Object>> optionsList = new List<Map<Object,Object>>();
        for(Map<String,Object> option : restaurantOptions){
            optionsList.add(
                new Map<Object,Object>{
                    'title' => option.get('title'),
                    'location' => new Map<Object,Object>{
                            'Latitude' => ((Map<String,String>)option.get('location')).get('Latitude'),
                            'Longitude' => ((Map<String,String>)option.get('location')).get('Longitude'),
                            'Distance' => ((Map<String,String>)option.get('location')).get('Distance'),
                            'PostalCode' => ((Map<String,String>)option.get('location')).get('PostalCode'),
                            'Street' => ((Map<String,String>)option.get('location')).get('Street'),
                            'City' => ((Map<String,String>)option.get('location')).get('City'),
                            'State' => ((Map<String,String>)option.get('location')).get('State')
                        },
                    'simpleDescription' => option.get('simpleDescription')
                }
            );
        }
        return optionsList;
    }


}