global class RestaurantFinderNotificationSchedule implements Schedulable {
    
    private static final String EMAIL_MSG = 'Hoping you are doing well, this message is to kindly remind you that there is a daily process to select a restaurant to lunch for every team. Please visit your Salesforce application Restaurant Finder to select today\'s option';

    global void execute(SchedulableContext ctx) {
        execute();
    }

    public void execute(){
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        List<User> teamMembers = [SELECT Id, Name, Email, RestaurantFinderTeam__c FROM User WHERE RestaurantFinderTeamLeader__c = false AND isActive = true AND RestaurantFinderTeam__c != null];
        for(User teamMember : teamMembers){
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setToAddresses(new List<String>{teamMember.Email});
            email.setSenderDisplayName('Salesforce Restaurant Finder App');
            email.setSubject('Restaurant Finder Notification');
            email.setBccSender(false);
            email.setUseSignature(false);
            email.setPlainTextBody('Dear' + teamMember.Name + '\n\n' + EMAIL_MSG + '\n\nRegards');
            email.setHtmlBody('Dear <b>' + teamMember.Name + ' </b> <br/><br/> ' +  EMAIL_MSG + '<br/><br/>Regards');
            emails.add(email);
        }
        Messaging.sendEmail(emails);
   }

}