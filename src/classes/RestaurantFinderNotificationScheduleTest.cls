@IsTest
public class RestaurantFinderNotificationScheduleTest {
	@IsTest
    static void shouldSendEmail(){
        Test.startTest();
        RestaurantFinderNotificationSchedule notificationMethod = new RestaurantFinderNotificationSchedule();
        notificationMethod.execute();
    	Integer invocations = Limits.getEmailInvocations();
		Test.stopTest();
        System.assertEquals(1, invocations, 'An email should be sent');
    }
}