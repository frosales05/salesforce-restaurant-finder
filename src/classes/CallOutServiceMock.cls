@isTest
global class CallOutServiceMock implements HttpCalloutMock {
    public String set_Body;
    public String contentType;
    public Integer statusCode;

    public static String foursquareMockResponse(String Id){
        return '{"meta": {"code": 200,"requestId": "5ac51d7e6a607143d811cecb"},"response": {"venues": [{"id": "'+Id+'","name": "Mr. Purple","location": {"address": "180 Orchard St","crossStreet": "btwn Houston & Stanton St","lat": 40.72173744277209,"lng": -73.98800687282996,"labeledLatLngs": [{"label": "display","lat": 40.72173744277209,"lng": -73.98800687282996}],"distance": 8,"postalCode": "10002","cc": "US","city": "New York","state": "NY","country": "United States","formattedAddress": ["180 Orchard St (btwn Houston & Stanton St)","New York, NY 10002","United States"]},"categories": [{"id": "4bf58dd8d48988d1d5941735","name": "Hotel Bar","pluralName": "Hotel Bars","shortName": "Hotel Bar","icon": {"prefix": "https://ss3.4sqi.net/img/categories_v2/travel/hotel_bar_","suffix": ".png"},"primary": true}],"venuePage": {"id": "150747252"}}]}}';    
    }
    global CallOutServiceMock(final String set_Body, final String contentType, final Integer statusCode){        
        this.set_Body = set_Body;
        this.contentType = contentType;
        this.statusCode = statusCode;
    }
    
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', contentType);
        response.setBody(set_Body);
        response.setStatusCode(statusCode);
        return response; 
    }
}