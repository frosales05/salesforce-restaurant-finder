public class RestaurantFinder {
    
    public static final String TEAM_DEVELOPER_NAME_PREFIX = 'RF_';
    public static final String GROUP_DEFAULT_TYPE = 'Regular';
    public static final String LINE_BREAK = '<br/>';
    public static final String BLANK_SPACE = ' ';
    public static final Date TODAY_DATE = Date.today();
    public static final String RESTAURANT_CHOICE_PREFIX = 'Restaurant Choice - ';
    public static final String RESTAURANT_CHOICE_SUFFIX = ' - Team ';
    public static final String CRON_EXPRESSION = '0 0 12 * * ?';
    public static final String CRON_NAME = 'Restaurant Finder Notificator';
    private static final String DEFAULT_CLIENT_ID = 'KNPWPMMBCXSPBHSV4LEJD5FW4EQMOR3ECTSEUMZODU2HRYEB';
    private static final String DEFAULT_CLIENT_SECRET = 'C5JNLRX1C0T25QMN0DWDA44I4F2Z452ST4QVXHRQUGHVQH1U';
    private static final String DEFAULT_API_VERSION = '20190225';
    private static final String DEFAULT_BASE_URL = 'https://api.foursquare.com/';
    private static final String DEFAULT_CATEGORY_ID = '4d4b7105d754a06374d81259'; 
    private static final String DEFAULT_ENDPOINT = 'v2/venues/search?';
    private static final String DEFAULT_LATITUDE_LONGITUDE = '35.6487634,139.7021532';
    private static final String DEFAULT_LUNCH_TIME = '12:00';
    
    @AuraEnabled
    public static Boolean validateTeamMembership(){
        String isTeamMember = [SELECT RestaurantFinderTeam__c FROM User WHERE Id = :UserInfo.getUserId()].RestaurantFinderTeam__c;
        return isTeamMember != null && String.isNotBlank(isTeamMember);
    }

    @AuraEnabled
    public static List<Map<String,Object>> getRestaurantOptions(String query, String radius){
        try{
            Map<String,Object> jsonMap = (Map<String,Object>)System.JSON.deserializeUntyped(getRequest(prepareEndpointURL(query, radius)));
            Map<String,Object> response = (Map<String,Object>)jsonMap.get('response');
            List<Object> venues = (List<Object>)response.get('venues');
            List<Map<String,Object>> restaurantOptions = new List<Map<String,Object>>();

            for(Object venue : venues){
                Map<String,Object> restaurantOption = new Map<String,Object>();
                Map<String,String> restaurantLocation = new Map<String,String>();
                Map<String,Object> venueMap = (Map<String,Object>)venue;
                Map<String,Object> locationMap = (Map<String,Object>)venueMap.get('location');
                List<Object> categoriesList = (List<Object>)venueMap.get('categories');
                String simpleDescription = '';
                
                restaurantLocation.put('Latitude',getAsString(locationMap.get('lat')));
                restaurantLocation.put('Longitude',getAsString(locationMap.get('lng')));
                restaurantLocation.put('Distance',getAsString(locationMap.get('distance')));
                restaurantLocation.put('PostalCode',getAsString(locationMap.get('postalCode')));
                restaurantLocation.put('Street',getAsString(locationMap.get('address')));
                restaurantLocation.put('City',getAsString(locationMap.get('city')));
                restaurantLocation.put('Country',getAsString(locationMap.get('country')));
                restaurantLocation.put('State',getAsString(locationMap.get('state')));
                
                if(!categoriesList.isEmpty()){
                    Map<String,Object> categoriesMap = (Map<String,Object>)categoriesList[0];
                    simpleDescription = getAsString(categoriesMap.get('name'));
                }
                
                restaurantOption.put('title', venueMap.get('name'));
                restaurantOption.put('id', venueMap.get('id'));
                restaurantOption.put('description', getFormatedDescription(simpleDescription, getAsString(locationMap.get('address')), getAsString(locationMap.get('state')), getAsString(locationMap.get('city')), getAsString(locationMap.get('postalCode')), getAsString(locationMap.get('country'))));
                restaurantOption.put('simpleDescription', simpleDescription);
                restaurantOption.put('location', restaurantLocation);
                restaurantOptions.add(restaurantOption);
            }
            
            return restaurantOptions;
        } catch(Exception e){
            throw new AuraHandledException('There is a problem with the API settings, please reach your System Administrator to solve this issue.');
        }
    
    }

    private static String getAsString(Object value){
        return String.valueOf(value);
    }

    private static Decimal getAsDecimal(Object value){
        return Decimal.valueOf(String.valueOf(value));
    }

    private static String getFormatedDescription(String simpleDescription, String address, String state, String city, String postalCode, String country){
        return simpleDescription + LINE_BREAK + address + LINE_BREAK + state + BLANK_SPACE + city + LINE_BREAK + postalCode + LINE_BREAK + country;
    }

    private static String prepareEndpointURL(String query, String radius){
        RestaurantFinderSettings__c apiSettings = [SELECT Client_Id__c, Client_Secret__c, API_Version__c, Base_URL__c, Category_Id__c, Endpoint__c, Initial_Latitude_Longitude__c FROM RestaurantFinderSettings__c LIMIT 1];
        return (
            apiSettings.Base_URL__c + apiSettings.Endpoint__c + 
            'client_id=' + apiSettings.Client_Id__c + 
            '&client_secret=' + apiSettings.Client_Secret__c + 
            '&v=' + apiSettings.API_Version__c +
            '&ll=' + apiSettings.Initial_Latitude_Longitude__c +
            '&query=' + query + 
            '&radius=' + radius +
            '&categoryId=' + apiSettings.Category_Id__c
        );
    }

    private static String getRequest(String endPointURL){

        HttpRequest reqData = new HttpRequest();
        Http http = new Http();

        reqData.setHeader('Content-Type','application/json');
        reqData.setHeader('Connection','keep-alive');
        reqData.setHeader('Content-Length','0');
        reqData.setTimeout(120000); 
        reqData.setEndpoint(endpointURL);
        reqData.setMethod('GET');

        HTTPResponse res = http.send(reqData);
        
        return res.getBody();
    
    }

    @AuraEnabled
    public static void saveRestaurantOption(Object restaurantOption){

        Map<Object,Object> restaurant = (Map<Object,Object>)restaurantOption;
        Map<Object,Object> restaurantLocation = (Map<Object,Object>)restaurant.get('location');
        RestaurantOption__c restaurantToSave = new RestaurantOption__c(
            Name = getAsString(restaurant.get('title')),
            RestaurantChoice__c = getCurrentRestaurantChoice(),
            TeamMember__c = UserInfo.getUserId(),
            Description__c = getAsString(restaurant.get('simpleDescription')),
            City__c = getAsString(restaurantLocation.get('City')),
            Country__c = getAsString(restaurantLocation.get('Country')),
            Location__latitude__s = getAsDecimal(restaurantLocation.get('Latitude')),
            Location__longitude__s = getAsDecimal(restaurantLocation.get('Longitude')),
            PostalCode__c = getAsString(restaurantLocation.get('PostalCode')),
            State__c = getAsString(restaurantLocation.get('State')),
            Street__c = getAsString(restaurantLocation.get('Street')),
            Distance__c = getAsDecimal(restaurantLocation.get('Distance'))
        );

        insert restaurantToSave;

    }

    private static Id getCurrentRestaurantChoice(){

        User currentUser = [SELECT Id, RestaurantFinderTeam__c FROM User WHERE Id = :UserInfo.getUserId()];
        List<RestaurantChoice__c> currentRestaurantChoice = [SELECT Id FROM RestaurantChoice__c WHERE DateOfChoice__c = TODAY AND Team__c = :currentUser.RestaurantFinderTeam__c];

        if(currentRestaurantChoice.isEmpty()){
            currentRestaurantChoice.add(createRestaurantChoice(currentUser.RestaurantFinderTeam__c));
        }

        return currentRestaurantChoice[0].Id;

    }

    @AuraEnabled
    public static RestaurantChoice__c createRestaurantChoice(String team){

        RestaurantChoice__c restaurantChoice = new RestaurantChoice__c(
            Name = RESTAURANT_CHOICE_PREFIX + TODAY_DATE + RESTAURANT_CHOICE_SUFFIX + team,
            Team__c = team,
            DateOfChoice__c = TODAY_DATE
        );

        insert restaurantChoice;
        
        return restaurantChoice;
    }

    @AuraEnabled
    public static Boolean checkIfTheUserAlreadyMadeAChoice(){
        return ![SELECT Id FROM RestaurantOption__c WHERE TeamMember__c = :UserInfo.getUserId() AND RestaurantChoice__r.DateOfChoice__c = :TODAY_DATE].isEmpty();
    }

    @AuraEnabled
    public static Boolean checkIfTheUserIsTeamLeader(){
        return [SELECT RestaurantFinderTeamLeader__c FROM User WHERE Id = :UserInfo.getUserId()].RestaurantFinderTeamLeader__c;
    }

    @AuraEnabled
    public static List<Map<String,Object>> fetchRestaurantOptions(){

        User teamLeader = [SELECT Id, RestaurantFinderTeam__c FROM User WHERE Id = : UserInfo.getUserId()];
        List<Map<String,Object>> restaurantTeamOptions = new List<Map<String,Object>>();

        for(RestaurantOption__c restaurantTeamOption : [SELECT Id, Name, Description__c, City__c, Country__c, Location__latitude__s, Location__longitude__s, PostalCode__c, State__c, Street__c, Distance__c FROM RestaurantOption__c WHERE RestaurantChoice__r.Team__c = :teamLeader.RestaurantFinderTeam__c]){
            Map<String,Object> restaurantOption = new Map<String,Object>();
            Map<String,String> restaurantLocation = new Map<String,String>();
            
            restaurantLocation.put('Latitude', getAsString(restaurantTeamOption.Location__latitude__s));
            restaurantLocation.put('Longitude', getAsString(restaurantTeamOption.Location__longitude__s));
            restaurantLocation.put('Distance', getAsString(restaurantTeamOption.Distance__c));
            restaurantLocation.put('PostalCode', restaurantTeamOption.PostalCode__c);
            restaurantLocation.put('Street', restaurantTeamOption.Street__c);
            restaurantLocation.put('City', restaurantTeamOption.City__c);
            restaurantLocation.put('Country', restaurantTeamOption.Country__c);
            restaurantLocation.put('State', restaurantTeamOption.State__c);
            restaurantOption.put('title', restaurantTeamOption.Name);
            restaurantOption.put('id', restaurantTeamOption.Id);
            restaurantOption.put('description', getFormatedDescription(restaurantTeamOption.Description__c, restaurantTeamOption.Street__c, restaurantTeamOption.State__c, restaurantTeamOption.City__c, restaurantTeamOption.PostalCode__c, restaurantTeamOption.Country__c));
            restaurantOption.put('location', restaurantLocation);
            restaurantTeamOptions.add(restaurantOption);
        }

        return restaurantTeamOptions;

    }

    @AuraEnabled
    public static void saveRestaurantChoice(String restaurantChoiceId){
        update new RestaurantChoice__c(
            Id = [SELECT RestaurantChoice__c FROM RestaurantOption__c WHERE Id = :restaurantChoiceId].RestaurantChoice__c,
            RestaurantSelected__c = restaurantChoiceId
        );
    }

    @AuraEnabled
    public static Boolean checkIfTheLeaderAlreadyMadeAChoice(){
        User teamLeader = [SELECT RestaurantFinderTeam__c FROM User WHERE Id = :UserInfo.getUserId()];
        return ![SELECT RestaurantSelected__c FROM RestaurantChoice__c WHERE DateOfChoice__c = :TODAY_DATE AND Team__c = :teamLeader.RestaurantFinderTeam__c AND RestaurantSelected__c != null].isEmpty();
    }

    @AuraEnabled
    public static RestaurantFinderSettings__c getAdministrationSettings(){
        List<RestaurantFinderSettings__c> restaurantFinderSettings = [SELECT Id, Client_Id__c, Client_Secret__c, API_Version__c, Base_URL__c, Category_Id__c, Endpoint__c, Initial_Latitude_Longitude__c, Lunch_Time__c FROM RestaurantFinderSettings__c];
        if(restaurantFinderSettings.isEmpty()){
            restaurantFinderSettings.add(
                new RestaurantFinderSettings__c(
                    Client_Id__c = DEFAULT_CLIENT_ID, 
                    Client_Secret__c = DEFAULT_CLIENT_SECRET, 
                    API_Version__c = DEFAULT_API_VERSION, 
                    Base_URL__c = DEFAULT_BASE_URL, 
                    Category_Id__c = DEFAULT_CATEGORY_ID, 
                    Endpoint__c = DEFAULT_ENDPOINT, 
                    Initial_Latitude_Longitude__c = DEFAULT_LATITUDE_LONGITUDE, 
                    Lunch_Time__c = DEFAULT_LUNCH_TIME
                )
            );
            insert restaurantFinderSettings;
        }
        return restaurantFinderSettings[0];
    }

    @AuraEnabled
    public static void updateAdministrationSettings(String settingsId, String clientId, String clientSecret, String apiVersion, String baseUrl, String categoryId, String endpoint, String initialLatitudeLongitude, String lunchTime){
        try{
            update new RestaurantFinderSettings__c(
                Id = settingsId,
                Client_Id__c = clientId,
                Client_Secret__c = clientSecret,
                API_Version__c = apiVersion, 
                Base_URL__c = baseUrl, 
                Category_Id__c = categoryId, 
                Endpoint__c = endpoint, 
                Initial_Latitude_Longitude__c = initialLatitudeLongitude,
                Lunch_Time__c = lunchTime
            );
        } catch(DMLException e) {
            for (Integer i = 0; i < e.getNumDml(); i++) {
                throw new AuraHandledException(e.getDmlMessage(i));
            }
        }

    }

    @AuraEnabled
    public static void scheduleNotification(String lunchTime){

        RestaurantFinderSettings__c restaurantFinderSettings = [SELECT Id, Notifier_Job_Id__c FROM RestaurantFinderSettings__c LIMIT 1];
        System.debug('restaurantFinderSettings' + restaurantFinderSettings);
        if(restaurantFinderSettings.Notifier_Job_Id__c != null && ![SELECT Id FROM CronTrigger WHERE Id = :restaurantFinderSettings.Notifier_Job_Id__c].isEmpty()){
            System.abortJob(restaurantFinderSettings.Notifier_Job_Id__c);
        }
        
        String[] lunchTimeSplitted = lunchTime.split(':');
        String cronExp = CRON_EXPRESSION;
        String cronName = CRON_NAME;
        
        if(lunchTimeSplitted.size() > 1){
            cronExp = '0 ' + lunchTimeSplitted[1] + ' ' + lunchTimeSplitted[0] + ' * * ?';
        }
        
        RestaurantFinderNotificationSchedule notificationScheduler = new RestaurantFinderNotificationSchedule();
        restaurantFinderSettings.Notifier_Job_Id__c = System.schedule(cronName, cronExp, notificationScheduler);
        
        update restaurantFinderSettings;

    }

    @AuraEnabled
    public static List<User> getActiveUsers(String teamDeveloperName){
        List<User> currentTeamMembers = getTeamMembersByTeamDeveloperName(teamDeveloperName);
        List<Id> currentTeamMemberIds = new List<Id>();
        List<Id> restaurantFinderTeamIds = new List<Id>();
        List<Id> userAssignedToOtherTeamIds = new List<Id>();
        Map<Id,Profile> profileIds = new Map<Id,Profile>([SELECT Id, UserLicenseId FROM Profile WHERE UserLicenseId NOT IN (SELECT Id FROM UserLicense WHERE Name LIKE '%Chatter%')]);
        
        for(Group team : getRestaurantFinderTeams()){
            restaurantFinderTeamIds.add(team.Id);
        }

        for(User currentTeamMember : currentTeamMembers){
            currentTeamMemberIds.add(currentTeamMember.Id);
        }

        for(GroupMember teamMember : [SELECT UserOrGroupId FROM GroupMember WHERE GroupId IN :restaurantFinderTeamIds]){
            userAssignedToOtherTeamIds.add(teamMember.UserOrGroupId);
        }

        return [SELECT Id, Name FROM User WHERE isActive = true AND ProfileId IN :profileIds.Keyset() AND (Id NOT IN :userAssignedToOtherTeamIds OR Id IN :currentTeamMemberIds)];
    }

    @AuraEnabled
    public static List<Group> getRestaurantFinderTeams(){
        return [SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName LIKE :TEAM_DEVELOPER_NAME_PREFIX + '%'];
    }

    @AuraEnabled
    public static List<User> getTeamMembersByTeamDeveloperName(String teamDeveloperName){
        try{
            List<GroupMember> teamMembers = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId IN (SELECT Id FROM Group WHERE DeveloperName = :teamDeveloperName)];
            List<Id> teamMemberIds = new List<Id>();
            for(GroupMember teamMember : teamMembers){
                teamMemberIds.add(teamMember.UserOrGroupId);
            }
            return [SELECT Id, Name FROM User WHERE Id IN :teamMemberIds];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());  
        }
    }

    @AuraEnabled
    public static void createTeam(String teamName){
        try{
            insert new Group(
                Name = teamName,
                Type = GROUP_DEFAULT_TYPE,
                DeveloperName = TEAM_DEVELOPER_NAME_PREFIX + teamName
            );
        } catch (DMLException e) {
            for (Integer i = 0; i < e.getNumDml(); i++) {
                throw new AuraHandledException(e.getDmlMessage(i));
            }
        }
    }

    @AuraEnabled
    public static String updateTeam(String teamDeveloperName, String teamNewName){
        try{
            Group teamToUpdate = [SELECT Id, OwnerId FROM Group WHERE DeveloperName = :teamDeveloperName];
            teamToUpdate.Name = teamNewName;
            teamToUpdate.DeveloperName = TEAM_DEVELOPER_NAME_PREFIX + teamNewName;
            
            update teamToUpdate;
            
            List<User> usersToUpdate = [SELECT Id FROM User WHERE RestaurantFinderTeam__c = :teamDeveloperName];
            
            for(User userToUpdate : usersToUpdate){
                userToUpdate.RestaurantFinderTeam__c = TEAM_DEVELOPER_NAME_PREFIX + teamNewName;
            }
            
            update usersToUpdate;

            List<RestaurantChoice__c> restaurantChoicesToUpdate = [SELECT Id, Name FROM RestaurantChoice__c WHERE Team__c = :teamDeveloperName];
            
            for(RestaurantChoice__c restaurantChoiceToUpdate : restaurantChoicesToUpdate){
                restaurantChoiceToUpdate.Team__c = TEAM_DEVELOPER_NAME_PREFIX + teamNewName;
                restaurantChoiceToUpdate.Name = restaurantChoiceToUpdate.Name.replace(teamDeveloperName, TEAM_DEVELOPER_NAME_PREFIX + teamNewName);
            }

            update restaurantChoicesToUpdate;
            
            return TEAM_DEVELOPER_NAME_PREFIX + teamNewName;
        
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void deleteTeam(String teamDeveloperName){
        try{
            List<User> userRelatedtoTheDeletedTeam = [SELECT Id FROM User WHERE RestaurantFinderTeam__c = :teamDeveloperName];
            for(User userRelated : userRelatedtoTheDeletedTeam){
                userRelated.RestaurantFinderTeam__c = null;
                userRelated.RestaurantFinderTeamLeader__c = false;
            }
            update userRelatedtoTheDeletedTeam;
            delete [SELECT Id FROM Group WHERE DeveloperName = :teamDeveloperName];
            deleteRestaurantChoice(teamDeveloperName);
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @future
    private static void deleteRestaurantChoice(String teamDeveloperName){
        delete [SELECT Id FROM RestaurantChoice__c WHERE Team__c = :teamDeveloperName];
    }

    @AuraEnabled
    public static void createTeamMembers(List<String> teamMemberIds, String teamDeveloperName){
        try{
            Id teamId = [SELECT Id FROM Group WHERE DeveloperName = :teamDeveloperName].Id;
            List<GroupMember> teamMembersToInsert = new List<GroupMember>();
            List<User> usersToUpdate = new List<User>();
            for(String teamMemberId : teamMemberIds){
                teamMembersToInsert.add(
                    new GroupMember(
                        UserOrGroupId = teamMemberId,
                        GroupId = teamId
                    )
                );
                usersToUpdate.add(
                    new User(
                        Id = teamMemberId,
                        RestaurantFinderTeam__c = teamDeveloperName
                    )
                );
            }
            insert teamMembersToInsert;
            update usersToUpdate;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }    
    }

    @AuraEnabled
    public static void markTeamLeader(String teamLeaderId, String teamDeveloperName){
        try{
            List<User> previousTeamLeaders = getTeamLeader(teamDeveloperName);
            for(User previousTeamLeader : previousTeamLeaders){
                previousTeamLeader.RestaurantFinderTeamLeader__c = false;
            }
            update previousTeamLeaders;
            update new User(
                Id = teamLeaderId,
                RestaurantFinderTeamLeader__c = true
            );
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<User> getTeamLeader(String teamDeveloperName){
        return [SELECT Id, Name FROM User WHERE RestaurantFinderTeam__c = :teamDeveloperName AND RestaurantFinderTeamLeader__c = true];
    }

    @AuraEnabled
    public static Map<String,Boolean> getTeamStatus(String teamDeveloperName){
        return new Map<String,Boolean>{
            'NoTeamLeaderYet' => getTeamLeader(teamDeveloperName).isEmpty(),
            'LessThan2TeamMembers' => getTeamMembersByTeamDeveloperName(teamDeveloperName).size() < 2
        };
    }

    @AuraEnabled
    public static void dropTeamMembers(List<String> teamMembersIdToDelete){
        try{
            List<User> listOfUserToUpdate = [SELECT Id FROM User WHERE Id IN :teamMembersIdToDelete];

            for(User userToUpdate : listOfUserToUpdate){
                userToUpdate.RestaurantFinderTeamLeader__c = false;
                userToUpdate.RestaurantFinderTeam__c = null;
            }

            update listOfUserToUpdate;
            
            List<GroupMember> listOfTeamMembersToDelete = [SELECT Id FROM GroupMember WHERE UserOrGroupId IN :teamMembersIdToDelete];
            
            delete listOfTeamMembersToDelete;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}