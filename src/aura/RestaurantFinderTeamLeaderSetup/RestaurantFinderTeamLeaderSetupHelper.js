({
	setTeamMembers : function(component, event, helper){
		this.toggleLoading(component);
		var teamMembers = [];
		for (var i = 0; i < component.get("v.options").length; i++) {
			if(component.get("v.values").includes(component.get("v.options")[i].value)){
				var item = {
					"Name": component.get("v.options")[i].label,
					"Id": component.get("v.options")[i].value
				};
				teamMembers.push(item);
			}
		}
		var sections = [
			{
				label : "Select a Team Leader",
				items : teamMembers
			}
		];
		component.set("v.teamMembers", sections);
		var action = component.get("c.getTeamLeader");
		action.setParams({
			"teamDeveloperName" : component.get("v.teamDeveloperName")
		});
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS"){
				component.set("v.selectedTeamLeader", '');
				if(response.getReturnValue().length > 0){
					component.set("v.selectedTeamLeader", response.getReturnValue()[0].Id);
				}
				this.checkButtonStatus(component, event, helper);
			} else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
		});
		$A.enqueueAction(action);
	},
    toggleLoading: function(component){
        var loader = component.find("loading");
        $A.util.toggleClass(loader,"slds-hide");
	},
	selectTeamLeader: function(component, event, helper){
		this.toggleLoading(component);
		var action = component.get("c.markTeamLeader");
		action.setParams({
			"teamLeaderId": component.get("v.selectedTeamLeader"),
			"teamDeveloperName": component.get("v.teamDeveloperName"),
		});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				component.set("v.teamLeaderSelected", true);
				this.updateTeamSetupComponentRequest(component, event, helper);
				this.successHandler(component, event, helper);
            } else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
        });
        $A.enqueueAction(action);
	},
	successHandler : function(component, event, helper){
		var toastEventSuccess = $A.get("e.force:showToast");
		toastEventSuccess.setParams({
			"title": "Success",
			"type": "success",
			"message": "All done"
		});
		toastEventSuccess.fire();
	},
	errorHandler : function(component, errorMessage){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Error",
			"type": "error",
			"mode": "sticky",
			"message": errorMessage[0].message
		});
		toastEvent.fire();
	},
	checkButtonStatus : function(component, event, helper){
		if(component.get("v.selectedTeamLeader") !== ''){
			component.set("v.disableButton", false);
		} else {
			component.set("v.disableButton", true);
		}
	},
	updateTeamSetupComponentRequest: function(component, event, helper){
		var appEvent = $A.get("e.c:RestaurantFinderTeamsEvent");
		appEvent.setParams({ 
			"updateTeamStatus" : true
		});
		appEvent.fire();
	},
	checkTeamStatus : function(component, event, helper){
		this.toggleLoading(component);
		var action = component.get("c.getTeamStatus");
		action.setParams({
            "teamDeveloperName": component.get("v.teamDeveloperName")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				var blockCard = '';
				if(response.getReturnValue().LessThan2TeamMembers){
					blockCard = true;
				} else {
					blockCard = false;
				}
				component.set("v.disableCard", blockCard);
            } else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
        });
        $A.enqueueAction(action);
	}
})