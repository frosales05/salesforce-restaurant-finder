({
	handleApplicationEvent: function(component, event, helper) {
		var teamWasUpdated = event.getParam("updateTeamLeader");
		var teamWasDeleted = event.getParam("cleanTeamMembers");
		var teamDeveloperName = event.getParam("teamDeveloperName");
		var teamMembersOptions = event.getParam("options");
		var teamMembersValues = event.getParam("values");
		if(teamWasUpdated){
			component.set("v.teamDeveloperName", teamDeveloperName);
			component.set("v.options", teamMembersOptions);
			component.set("v.values", teamMembersValues);
			helper.setTeamMembers(component, event, helper);
		}
		if(teamWasDeleted){
			component.set("v.options", '');
			component.set("v.values", '');
			component.set("v.teamMembers", '');
			component.set("v.selectedTeamLeader", '')
			helper.checkButtonStatus(component, event, helper);
		}
		helper.checkTeamStatus(component, event, helper);
	},
	selectTeamLeader: function(component, event, helper){
		helper.selectTeamLeader(component, event, helper);
	},
	checkButtonStatus: function(component, event, helper){
		helper.checkButtonStatus(component, event, helper);
	}
})