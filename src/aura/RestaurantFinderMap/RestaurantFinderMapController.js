({
    init: function (component, event, helper) {
        component.set('v.mapMarkerSelected', [
            {
                location: {
                    Latitude : '35.6487634',
                    Longitude : '139.7021532'
                },
                title: 'Cogent Labs',
                description: 'The best place to work!'
            }
        ]);
    },
    toggleLoading: function(component){
        var loader = component.find("loading");
        $A.util.toggleClass(loader,"slds-hide");
	},
	errorHandler : function(component, errorMessage){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Error",
			"type": "error",
			"mode": "sticky",
			"message": errorMessage[0].message
		});
		toastEvent.fire();
    },
    handleApplicationEvent: function(component, event, helper){
        var showMapMarker = event.getParam("showMapMarker");
        var selectedMapMarker = event.getParam("selectedMapMarker");
        var mapMarkers = event.getParam("mapMarkers");
		if(showMapMarker){
            component.set("v.mapMarkers", mapMarkers);
            var items = [];
            for (var i = 0; i < mapMarkers.length; i++) {
                if(mapMarkers[i].id === selectedMapMarker){
                    items.push(mapMarkers[i]);
                    component.set("v.mapMarkerSelected", items);
                }
            }
		}
    }
})