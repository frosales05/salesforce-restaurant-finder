({
    onFireChange : function(component){
        var method = component.get("v.onChangeMethod");
        if (method) {
            $A.enqueueAction(method);
        }
    }
})