({
	checkTeamMembership: function(component, event, helper){
    	var action = component.get("c.validateTeamMembership");
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS"){
				component.set("v.teamMembershipValid", response.getReturnValue());
                if(response.getReturnValue() === true){
                	this.checkIfIsTeamLeader(component, event, helper); 
                }
			} else {
				helper.errorHandler(component, response.getError());
			}
		});
		$A.enqueueAction(action);  
    },
    checkIfIsTeamLeader: function(component, event, helper){
		var action = component.get("c.checkIfTheUserIsTeamLeader");
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS"){
				component.set("v.isTeamLeader", response.getReturnValue());
				if(component.get("v.isTeamLeader")){
					this.getRestaurantTeamOptions(component, event, helper);
				} else {
					this.loadRestaurants(component, event, helper, component.get("v.query"), component.get("v.radius"));
				}
			} else {
				helper.errorHandler(component, response.getError());
			}
		});
		$A.enqueueAction(action);
	},
	getRestaurantTeamOptions: function(component, event, helper){
		var action = component.get("c.fetchRestaurantOptions");
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS"){
				component.set("v.mapMarkers", response.getReturnValue());
				this.checkIfTheRestaurantWasAlreadySelectedByTheLeader(component, event, helper);
				if(response.getReturnValue().length > 0){
					this.enableRandomButton(component, event, helper);
				}
			} else {
				helper.errorHandler(component, response.getError());
			}
		});
		$A.enqueueAction(action);
	},
	checkIfTheRestaurantWasAlreadySelectedByTheLeader: function(component, event, helper){
		var action = component.get("c.checkIfTheLeaderAlreadyMadeAChoice");
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS"){
				component.set("v.restaurantChoiceAlreadySelected", response.getReturnValue());
				if(response.getReturnValue()){
					this.disableSaveButton(component, event, helper);
					this.disableRandomButton(component, event, helper);
				}
			} else {
				helper.errorHandler(component, response.getError());
			}
		});
		$A.enqueueAction(action);
	},
	loadRestaurants: function(component, event, helper, query, radius){
		var action = component.get("c.getRestaurantOptions");
		action.setParams({
			"query" : query,
			"radius" : radius 
		});
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS"){
				component.set("v.mapMarkers", response.getReturnValue());
				this.checkIfTheRestaurantWasAlreadySelected(component, event, helper);
				if(response.getReturnValue().length > 0){
					this.enableRandomButton(component, event, helper);
				}
			} else {
				this.errorHandler(component, response.getError());
			}
		});
		$A.enqueueAction(action);
	},
	checkIfTheRestaurantWasAlreadySelected: function(component, event, helper){
		var action = component.get("c.checkIfTheUserAlreadyMadeAChoice");
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS"){
				component.set("v.restaurantAlreadySelected", response.getReturnValue());
				if(response.getReturnValue()){
					this.disableSaveButton(component, event, helper);
					this.disableRandomButton(component, event, helper);
				}
			} else {
				helper.errorHandler(component, response.getError());
			}
		});
		$A.enqueueAction(action);
	},
	saveSelectedRestaurantOption: function(component, event, helper){
		this.toggleLoading(component);
		var selectedRestaurant;
		var mapMarkers = component.get("v.mapMarkers");
		for (var i = 0; i < mapMarkers.length; i++) {
			if(mapMarkers[i].id === component.get("v.selectedMapMarker")){
				selectedRestaurant = mapMarkers[i];
			}
		}
		var action = component.get("c.saveRestaurantOption");
		action.setParams({
			"restaurantOption" : selectedRestaurant
		});
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS"){
				this.successHandler(component, event, helper);
				this.checkIfTheRestaurantWasAlreadySelected(component, event, helper);
			} else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
		});
		$A.enqueueAction(action);
	},
	saveSelectedRestaurantByTeamLeader: function(component, event, helper){
		this.toggleLoading(component);
		var action = component.get("c.saveRestaurantChoice");
		action.setParams({
			"restaurantChoiceId" : component.get("v.selectedMapMarker")
		});
		action.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS"){
				this.successHandler(component, event, helper);
				this.checkIfTheRestaurantWasAlreadySelectedByTheLeader(component, event, helper);
			} else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
		});
		$A.enqueueAction(action);
	},
	sendSelectedMarkerToMap: function(component, event, helper){
		var appEvent = $A.get("e.c:RestaurantFinderMapEvent");
		appEvent.setParams({ 
			"showMapMarker" : true,
			"selectedMapMarker" : component.get("v.selectedMapMarker"),
			"mapMarkers" : component.get("v.mapMarkers")
		});
		appEvent.fire();
	},
	toggleLoading: function(component){
		var loader = component.find("loading");
		$A.util.toggleClass(loader,"slds-hide");
	},
	successHandler : function(component, event, helper){
		var toastEventSuccess = $A.get("e.force:showToast");
		toastEventSuccess.setParams({
			"title": "Success",
			"type": "success",
			"message": "All done"
		});
		toastEventSuccess.fire();
	},
	errorHandler : function(component, errorMessage){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Error",
			"type": "error",
			"mode": "sticky",
			"message": errorMessage[0].message
		});
		toastEvent.fire();
	},
	enableSaveButton: function(component, event, helper){
		component.set("v.disableButton", false);
	},
	disableSaveButton: function(component, event, helper){
		component.set("v.disableButton", true);
	},
	enableRandomButton: function(component, event, helper){
		component.set("v.disableRandom", false);
	},
	disableRandomButton: function(component, event, helper){
		component.set("v.disableRandom", true);
	}
})