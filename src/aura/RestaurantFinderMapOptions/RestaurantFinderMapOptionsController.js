({
    init: function (component, event, helper) {
        helper.checkTeamMembership(component, event, helper);
	},
	selectRestaurant : function(component, event, helper){
		helper.enableSaveButton(component, event, helper);
		helper.sendSelectedMarkerToMap(component, event, helper);
	},
	selectRandom: function(component, event, helper){
		var selectedRestaurant;
		var mapMarkers = component.get("v.mapMarkers");
		var randomRestaurant = Math.floor((Math.random() * mapMarkers.length));
		for (var i = 0; i < mapMarkers.length; i++) {
			if(i === randomRestaurant){
				selectedRestaurant = mapMarkers[i].id;
				component.set("v.selectedMapMarker", selectedRestaurant);
				helper.enableSaveButton(component, event, helper);
				helper.sendSelectedMarkerToMap(component, event, helper);
			}
		}
	},
	saveSelectedRestaurant: function(component, event, helper){
		if(component.get("v.isTeamLeader")){
			helper.saveSelectedRestaurantByTeamLeader(component, event, helper);
		} else {
			helper.saveSelectedRestaurantOption(component, event, helper);
		}
	},
	searchRestaurants: function(component, event, helper) {
		var radius = 5000;
		var query = component.find('enter-search').get('v.value');
		if(query === ''){
			helper.loadRestaurants(component, event, helper, component.get("v.query"), component.get("v.radius"));
		}
		helper.loadRestaurants(component, event, helper, query, radius);
    }
})