({
    onInit: function(component, event, helper) {
        helper.loadAdministrationSettings(component, event, helper);
    },
    onUpdate: function(component, event, helper) {
        helper.updateAdministrationSettings(component, event, helper);
    },
    changeToRecordUpdate: function(component, event, helper) {
        helper.changeToRecordUpdate(component, event, helper);
    },
    cancelEdition: function(component, event, helper) {
        helper.changeToRecordDetail(component, event, helper);
        helper.loadAdministrationSettings(component, event, helper);
    }
})