({
    loadAdministrationSettings: function(component, event, helper){
        var action = component.get("c.getAdministrationSettings");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var currentAdminstrationSettings = response.getReturnValue();
                component.set("v.restaurantFinderSettings", currentAdminstrationSettings);
            } else {
                this.errorHandler(component, response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    updateAdministrationSettings: function(component, event, helper) {
        this.toggleLoading(component);
        var action = component.get("c.updateAdministrationSettings");
        action.setParams({
            "settingsId": component.get("v.restaurantFinderSettings.Id"),
            "clientId": component.get("v.restaurantFinderSettings.Client_Id__c"),
            "clientSecret": component.get("v.restaurantFinderSettings.Client_Secret__c"),            
            "apiVersion": component.get("v.restaurantFinderSettings.API_Version__c"),
            "baseUrl": component.get("v.restaurantFinderSettings.Base_URL__c"),
            "categoryId": component.get("v.restaurantFinderSettings.Category_Id__c"),
            "endpoint": component.get("v.restaurantFinderSettings.Endpoint__c"),
            "initialLatitudeLongitude": component.get("v.restaurantFinderSettings.Initial_Latitude_Longitude__c"),
            "lunchTime": component.get("v.restaurantFinderSettings.Lunch_Time__c")            
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                this.updateScheduledNotification(component, event, helper);
                this.changeToRecordDetail(component, event, helper);
            } else {
                this.errorHandler(component, response.getError());
            }
            this.toggleLoading(component);
        });
        $A.enqueueAction(action);
    },
    updateScheduledNotification: function(component, event, helper){
        var action = component.get("c.scheduleNotification");
        action.setParams({
            "lunchTime": component.get("v.restaurantFinderSettings.Lunch_Time__c")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                console.log('Notification scheduled');
            } else {
                this.errorHandler(component, response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    changeToRecordUpdate: function(component, event, helper){        
        component.set("v.isRecordDetail",false);
    },
    changeToRecordDetail: function(component, event, helper){
        component.set("v.isRecordDetail",true);
    },
    toggleLoading: function(component){
        var loader = component.find("loading");
        $A.util.toggleClass(loader,"slds-hide");
    },
	errorHandler : function(component, errorMessage){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Error",
			"type": "error",
			"mode": "sticky",
			"message": errorMessage[0].message
		});
		toastEvent.fire();
	}
})