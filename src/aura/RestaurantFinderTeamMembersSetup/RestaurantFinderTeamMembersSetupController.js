({
	handleChange: function (component, event, helper) {
        var selectedOptionValue = event.getParam("value");
		helper.checkButtonStatus(component, event, helper);
		helper.updateTeamLeaderComponentRequest(component, event, helper);
    },
    handleApplicationEvent: function(component, event, helper) {
		var teamWasUpdated = event.getParam("updateTeamMembers");
		var teamWasDeleted = event.getParam("cleanTeamMembers");
		var teamDeveloperName = event.getParam("teamDeveloperName");
        if(teamWasUpdated){
			component.set("v.teamDeveloperName", teamDeveloperName);
			helper.loadUsersAvailable(component, event, helper);
			helper.loadUsersRelatedToTheTeam(component, event, helper);
		}
		if(teamWasDeleted){
			component.set("v.options", '');
			component.set("v.values", '');
			helper.checkButtonStatus(component, event, helper);
		}
	},
	saveTeamMembers: function(component, event, helper){
		helper.saveTeamMembers(component, event, helper);
	},
	selectTeamLeader: function(component, event, helper){
		helper.selectTeamLeader(component, event, helper);
	}
})