({
	loadUsersAvailable : function(component, event, helper) {
		var action = component.get("c.getActiveUsers");
		action.setParams({
			"teamDeveloperName": component.get("v.teamDeveloperName")
		});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				var items = [];
				for (var i = 0; i < response.getReturnValue().length; i++) {
					var item = {
						"label": response.getReturnValue()[i].Name,
						"value": response.getReturnValue()[i].Id,
					};
					items.push(item);
				}
				component.set("v.options", items);
            } else {
				this.errorHandler(component, response.getError());
			}
        });
        $A.enqueueAction(action);
	},
	loadUsersRelatedToTheTeam : function(component, event, helper) {
		this.toggleLoading(component);
		var action = component.get("c.getTeamMembersByTeamDeveloperName");
		action.setParams({
			"teamDeveloperName" : component.get("v.teamDeveloperName")
		});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				var items = [];
				for (var i = 0; i < response.getReturnValue().length; i++) {
					var item = response.getReturnValue()[i].Id;
					items.push(item);
				}
				component.set("v.values", items);
				this.checkButtonStatus(component, event, helper);
				this.updateTeamLeaderComponentRequest(component, event, helper);
            } else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
        });
        $A.enqueueAction(action);
	},
    toggleLoading: function(component){
        var loader = component.find("loading");
        $A.util.toggleClass(loader,"slds-hide");
	},
	saveTeamMembers: function(component, event, helper){
		this.toggleLoading(component);
		var action = component.get("c.createTeamMembers");
		action.setParams({
			"teamMemberIds": component.get("v.values"),
			"teamDeveloperName": component.get("v.teamDeveloperName")
		});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				this.deleteTeamMembers(component, event, helper);
				this.loadUsersAvailable(component, event, helper);
				this.loadUsersRelatedToTheTeam(component, event, helper);
				this.updateTeamSetupComponentRequest(component, event, helper);
				this.successHandler(component, event, helper);
            } else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
        });
        $A.enqueueAction(action);
	},
	deleteTeamMembers: function(component, event, helper){
		var items = [];
		for (var i = 0; i < component.get("v.options").length; i++) {
			if(!component.get("v.values").includes(component.get("v.options")[i].value)){
				items.push(component.get("v.options")[i].value);
			}
		}
		var action = component.get("c.dropTeamMembers");
		action.setParams({
			"teamMembersIdToDelete": items
		});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				this.successHandler(component, event, helper);
            } else {
				this.errorHandler(component, response.getError());
			}
        });
        $A.enqueueAction(action);
	},
	successHandler : function(component, event, helper){
		var toastEventSuccess = $A.get("e.force:showToast");
		toastEventSuccess.setParams({
			"title": "Success",
			"type": "success",
			"message": "All done"
		});
		toastEventSuccess.fire();
	},
	errorHandler : function(component, errorMessage){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Error",
			"type": "error",
			"mode": "sticky",
			"message": errorMessage[0].message
		});
		toastEvent.fire();
	},
	checkButtonStatus : function(component, event, helper){
		if(component.get("v.values").length >= 2){
			component.set("v.disableButton", false);
		} else {
			component.set("v.disableButton", true);
		}
	},
    updateTeamLeaderComponentRequest: function(component, event, helper){
		var appEvent = $A.get("e.c:RestaurantFinderTeamsEvent");
		appEvent.setParams({ 
			"updateTeamLeader" : true,
			"options" : component.get("v.options"),
			"values" : component.get("v.values"),
			"teamDeveloperName" : component.get("v.teamDeveloperName")
		});
		appEvent.fire();
	},
	updateTeamSetupComponentRequest: function(component, event, helper){
		var appEvent = $A.get("e.c:RestaurantFinderTeamsEvent");
		appEvent.setParams({ 
			"updateTeamStatus" : true
		});
		appEvent.fire();
	}
})