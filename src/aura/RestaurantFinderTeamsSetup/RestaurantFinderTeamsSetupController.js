({
	onInit: function (component, event, helper) {
		helper.getTeams(component, event, helper);
   	},
   	insertTeams: function (component, event, helper) {
		helper.insertTeams(component, event, helper);
		helper.getTeams(component, event, helper);
		helper.closeInsertModal(component,event,helper);
	},
	updateTeams : function(component, event, helper){
		helper.updateTeams(component, event, helper);
		helper.getTeams(component, event, helper);
		helper.closeEditModal(component,event,helper);
	},
	deleteTeams: function(component, event, helper){
		helper.deleteTeams(component, event, helper);
		helper.getTeams(component, event, helper);
		helper.closeDeleteModal(component,event,helper);
	},
	closeInsertModal: function(component,event,helper){    
        helper.closeInsertModal(component,event,helper);
    },
    openInsertModal: function(component,event,helper) {
		helper.openInsertModal(component,event,helper);
    },
	closeEditModal: function(component,event,helper){    
        helper.closeEditModal(component,event,helper);
    },
    openEditModal: function(component,event,helper) {
        helper.openEditModal(component,event,helper);
	},
	closeDeleteModal: function(component,event,helper){    
        helper.closeDeleteModal(component,event,helper);
    },
    openDeleteModal: function(component,event,helper) {
        helper.openDeleteModal(component,event,helper);
	},
	handleTeamSelection : function(component,event,helper){
		helper.checkButtonStatus(component,event,helper);
		helper.updateTeamMembersComponentRequest(component, event, helper);
		helper.checkTeamStatus(component,event,helper);
	},
	handleApplicationEvent: function(component, event, helper) {
		var updateTeamStatus = event.getParam("updateTeamStatus");
		if(updateTeamStatus){
			helper.checkTeamStatus(component, event, helper);
		}
	}
})