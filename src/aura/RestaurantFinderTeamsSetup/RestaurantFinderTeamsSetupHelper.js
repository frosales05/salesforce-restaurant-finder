({
	getTeams : function(component, event, helper) {
		var action = component.get("c.getRestaurantFinderTeams");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				var sections = [
					{
					  label: "Teams",
					  items: response.getReturnValue()
					}
				];
				component.set("v.teams", sections);
				this.checkButtonStatus(component);
            } else {
				this.errorHandler(component, response.getError());
			}
        });
        $A.enqueueAction(action);
	},
	insertTeams : function(component, event, helper) {
		this.toggleLoading(component);
		var action = component.get("c.createTeam");
		action.setParams({
            "teamName": component.get("v.teamName")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				var sections = [
					{
					  label: "Teams",
					  items: response.getReturnValue()
					}
				];
				component.set("v.teams", sections);
				this.successHandler(component, event, helper);
            } else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
        });
        $A.enqueueAction(action);
	},
	updateTeams : function(component, event, helper){
		this.toggleLoading(component);
		var action = component.get("c.updateTeam");
		action.setParams({
			"teamNewName": component.get("v.teamName"),
			"teamDeveloperName": component.get("v.selectedTeam")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				component.set("v.selectedTeam", response.getReturnValue());
				this.successHandler(component, event, helper);
            } else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
        });
        $A.enqueueAction(action);
	},
	deleteTeams : function(component, event, helper){
		this.toggleLoading(component);
		var action = component.get("c.deleteTeam");
		action.setParams({
            "teamDeveloperName": component.get("v.selectedTeam")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				component.set("v.selectedTeam", '');
				this.successHandler(component, event, helper);
				this.cleanTeamMembersComponentRequest(component, event, helper);
            } else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
        });
        $A.enqueueAction(action);
	},
	closeInsertModal: function(component,event,helper){    
        var cmpTarget = component.find('InsertModalbox');
		var cmpBack = component.find('Modalbackdrop');
		component.set("{!v.teamName}", '');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    openInsertModal: function(component,event,helper) {
        var cmpTarget = component.find('InsertModalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
	},
	closeEditModal: function(component,event,helper){    
        var cmpTarget = component.find('EditModalbox');
		var cmpBack = component.find('Modalbackdrop');
		component.set("{!v.teamName}", '');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    openEditModal: function(component,event,helper) {
        var cmpTarget = component.find('EditModalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
	},
	closeDeleteModal: function(component,event,helper){    
        var cmpTarget = component.find('DeleteModalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    openDeleteModal: function(component,event,helper) {
        var cmpTarget = component.find('DeleteModalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
	},
	successHandler : function(component, event, helper){
		var toastEventSuccess = $A.get("e.force:showToast");
		toastEventSuccess.setParams({
			"title": "Success",
			"type": "success",
			"message": "All done"
		});
		toastEventSuccess.fire();
	},
	errorHandler : function(component, errorMessage){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title": "Error",
			"type": "error",
			"mode": "sticky",
			"message": errorMessage[0].message
		});
		toastEvent.fire();
	},
	checkButtonStatus : function(component,event,helper){
		if(component.get("v.selectedTeam") === ''){
			component.set("v.disableButton", true);
		} else {
			component.set("v.disableButton", false);
		}
	},
	checkTeamStatus : function(component, event, helper){
		this.toggleLoading(component);
		var action = component.get("c.getTeamStatus");
		action.setParams({
            "teamDeveloperName": component.get("v.selectedTeam")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
				component.set("v.noTeamLeaderYet", response.getReturnValue().NoTeamLeaderYet);
				component.set("v.lessThan2TeamMembers", response.getReturnValue().LessThan2TeamMembers);
				var teamPercentage = '';
				if(response.getReturnValue().NoTeamLeaderYet && response.getReturnValue().LessThan2TeamMembers){
					teamPercentage = 30;
				} else if(response.getReturnValue().NoTeamLeaderYet || response.getReturnValue().LessThan2TeamMembers) {
					teamPercentage = 70;
				} else {
					teamPercentage = 100;
				}
				component.set("v.teamPercentage", teamPercentage);
            } else {
				this.errorHandler(component, response.getError());
			}
			this.toggleLoading(component);
        });
        $A.enqueueAction(action);
	},
    toggleLoading: function(component){
        var loader = component.find("loading");
        $A.util.toggleClass(loader,"slds-hide");
    },
    updateTeamMembersComponentRequest: function(component, event, helper){
		if(component.get("v.previousSelectedTeam") !== component.get("v.selectedTeam")){
			var appEvent = $A.get("e.c:RestaurantFinderTeamsEvent");
			appEvent.setParams({ 
				"updateTeamMembers" : true,
				"teamDeveloperName" : component.get("v.selectedTeam")
			});
			appEvent.fire();
		}
		component.set("v.previousSelectedTeam", component.get("v.selectedTeam"));
	},
	cleanTeamMembersComponentRequest: function(component, event, helper){
		var appEvent = $A.get("e.c:RestaurantFinderTeamsEvent");
		appEvent.setParams({ 
			"cleanTeamMembers" : true 
		});
		appEvent.fire();
	}
})