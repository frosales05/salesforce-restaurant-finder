# Salesforce Restaurant Finder

- [Salesforce Restaurant Finder](#salesforce-restaurant-finder)
	- [Problem and solution](#problem-and-solution)
	- [Installing the package in your Salesforce Org](#installing-the-package-in-your-salesforce-org)
        - [1. Instance modifications before installing](#1.-instance-modifications-before-pushing-code)
		- [2. Go to the package URL](#2.-go-to-the-package-url)
        - [3. Post-install tasks](#3.-post-install-tasks)
    - [Using the app](#using-the-app)
		- [1. Team setup](#1.-team-setup)
        - [2. Team members restaurant selection](#2.-team-members-restaurant-selection)
		- [3. Team leaders restaurant selection](#3.-team-leaders-restaurant-selection)
	- [Technical choices](#technical-choices)
	- [Trade-offs](#trade-offs)
	- [LinkedIn profile](#linkedin-profile)
---
## Problem and solution

The customers are always having trouble picking what to eat for lunch. They would like to have a Salesforce application which can help them pick a restaurant close to the office. 
Sometimes they will be tipped off about an awesome restaurant, so they would also like to be able to do a keyword search for restaurants in the area.

To solve this issue i made this package that can be installed in a Salesforce Organization with *Lightning Experience* enabled
and ready to use *Lightning Components*.

The app has an administration panel that allows the System Admin to manage the API settings in case that is needed and also create teams, team members and team leaders.

The team members can use *Restaurant Finder* to choose from a collection of restaurants that are displayed automatically. 
Once they made their choise, the team leader must review the options that the team members saved for the day and select one of them to go for lunch with the whole team.

Every day at an hour determined by the System Admin, the team members receive an automated email as a friendly reminder that they can access to this app to choose the restaurant of the day.

Finally, every user can access to a dashboard that shows every day's options and selected restaurants.

---
## Installing the package in your Salesforce Org

To install this package in your instance you need to execute the following steps

### 1. Instance modifications before installing

* Follow the [explanation](https://help.salesforce.com/HTViewHelpDoc?id=domain_name_overview.htm&language=en_US) given by Salesforce itself to enable the feature *My Domain* in order to be able to use *Lightning Components*


### 2. Go to the package URL

* By clicking [here](https://login.salesforce.com/packaging/installPackage.apexp?p0=04t1Q00000126MS) you will be redirected to the salesforce login

* After this you will see the steps to install the package
    
* Make sure that you accept the permissions request for the foursquare API and that you install the app for all the users
    

### 3. Post-install tasks

* Go to *Setup* -> *Users* and choose your app adminstrator by clicking the checkbox named *Restaurant Finder Org Administrator*

---
## Using the app

These are the basics you need to know:

### 1. Team setup

Go to the Restaurant Finder Application and click on the *Administration* tab

There you will see 4 cards

* The top one is to set all the API settings such as *client id* and *client secret*, among others.

* Below the previous one, at your left you will see the *Team setup* that alows you to create, edit and delete teams in your organization.

* The middle one is the *Team members setup* that will let you add/remove users from the previously created teams.

* The final card is the *Team leader setup* where you must select a team members that will be in charge of making the final call deciding which restaurant is the best choice to go for with his team.

### 2. Team members restaurant selection

Additionally to the Administration tab, you will see right next to it the *Restaurant Finder* tab that must be used by the team members.

Inside that tab you will see a map and a box with different restaurants that are available at max 1 KM around the primary spot (This spot is designated by the Admin org with the *Initial Latitude & Longitude*).

If you type some characters on the search box, the search engine will look for that word in a 5KM radius.

You can select any of the available restaurant with the *Random* button or you can just pick one and click it.

To finish the job you must click the *Save* button.

### 3. Team leaders restaurant selection

The team leaders use the same tab mentioned in the previous point.

The bigger difference is that here they will only see the restaurants that their team members chosen.

They need to select one to finish. This choise will be visible for all the team in the final tab *Restaurant Dashboards*
where every team member will be able to see the options and the chosen restaurant.

The system admin can see every team decision on this same dashboard.

---
## Technical choices

* The team administration is based on *public groups* that are created automatically by using the *Administration* tab
    * I did this instead of using Salesforce Roles because that way the app is not invasive at all with the hierarchy that might be already established at this point
    * Also this provides the hability to do quick changes to the teams without needing any Sharing settings or other administrative tasks that could be required

* The whole app is built over Lightning Components
	* This allows you to change the design of the pages very easily in the Lightning app builder
	* You can take advantage of the variety of components available, without having to do everything from scratch	

---
### Trade-offs

* The notification 
	* The message is a plain text sended through salesforce email service. 
	* With a little more time this could be made manageable by the system admin. 
	* For now it is not editable.

* The Dashboard
	* There are only 2 reports on the dashboard
	* With more time this could have been made with some more filters and charts
	
* The mobile version
	* For some reason that i still can't figure out, when the lightning components that have scroll enabled go into a smartphone screen, they loose the scroll
	* If you use desktop version of Salesforce1 or Lightning Experience, everything works fine
	
* The UI
	* UI could be a lot better with more work
	
* Testing
	* The are only unit tests for every method
	* With more time there could be business tests although there is 88% of code coverage
	
* React
	* Due to my lack of experience with React and the short timing, i couldn't use it for this project
	
---
### LinkedIn profile
[Click Here](https://www.linkedin.com/in/felipe-orlando-rosales-diaz/)